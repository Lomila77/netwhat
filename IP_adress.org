* _Netwhat:_


** _IP_Adress:_

Une adresse IP (Internet Protocol) est un numero d'identification qui est
attribue de facon permanente ou provisoire a chaque [[file:~/netwhat/definitions/peripherique.org][peripherique]] relie a un
[[file:~/netwhat/definitions/reseau_informatique.org][reseau informatique]] qui utilise l'[[file:~/netwhat/definitions/internet_protocole.org][Internet Protocol]](IP). L'adresse IP est a
la base du syssteme d'acheminement ([[file:~/netwhat/definitions/routage.org][routage]]) des [[file:~/netwhat/definitions/paquets.org][paquets de donnees]] sur
Internet.

Il existe des adresses IP de [[file:~/netwhat/definitions/version_4.org][version 4]] sur 32 bits, et de version 6 sur 128
bits. La version 4 est actuellement la plus utilisee : elle est generalement
representee en notation decimale avec quatres nombres compris entre
_0 et 255_, separes par des points, ce qui donne par exemple : "172.16.254.1".

L'adresse IP est code sur 32 bits (soit 4 octets) et est ecrite en decimale
pointee, ainsi, elle separe les 4 octets sous forme de 4 chiffres decimaux
allant de 0 a 225. Elle est indisociable du [[file:~/netwhat/definitions/masque_de_sous_reseau.org][masque de sous reseau]] qui la
separe en 2, d'une part l'adresse reseau, de l'autre, l'adresse de la machine.

Parmis la plage d'adresse definie par l'adresse IP et son masque, il y a
deux adresses particuliere, la premiere et la derniere.

*** _Adresse specifique :_

La premiere adresse est l'adresse du reseau et la derniere est l'adresse
de broadcast.
