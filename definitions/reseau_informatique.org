* _Reseau Informatique_

Un reseau informatique (data communication network - _DCN_) est un ensemble d'equipements relies entre eux pour echanger des informations. Comme un filet, le noeud est l'extremite d'une connexion, qui peut etre une intersection de plusieurs connexions et / ou d'equipements.

A l'horizontale, un reseau est une strate de _trois couche_ ([[file:~/netwhat/definitions/modele_OSI.org][OSI]]) : les infrastructures, les fonctions de controle et de commande, les services rendus a l'utilisateur.

A la verticale, on utilisera un decoupage geographique : reseau local, reseau d'acces et reseau d'interconnexion.
