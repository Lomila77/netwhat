* _Adresse Mac:_

L'adresse MAC est l'adresse de la carte reseau de la machine.
Elle est "l'identifiant" d'une machine sur un reseau et permet d'etre reconnues
et de reconnaitre les machines connecter sur le reseau.

Elle s'ecrit en hexadecimale et elle est code sur 6 octets, soit 48 [[file:~/netwhat/definitions/bit.org][bits]].
Elle constitue la partie inferieur de la [[file:~/netwhat/definitions/couche_laision_donnees.org][couche de liaison]].

Il existe une adresse MAC particuliere qui voie tout ses bits a 1, c'est
l'adresse de broadcast. Cette adresse permet d'identifier n'importe quelle
carte reseau. L'adresse de broadcast est l'adresse de destination contenue
dans le header de la [[file:~/netwhat/definitions/trames.org][trame]]. L'adresse de broadcast est commune a toute les
machines. Elle sert a envoyer un message a n'importe quelle machine.
